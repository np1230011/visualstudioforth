﻿#include <iostream>
#include <time.h>

int main()
{
    const int N = 4;
    int array[N][N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << ' ';
        }
        std::cout << "\n";
    }
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int remains = (buf.tm_mday % N);
    int summ = 0;
    for (int i = 0; i < N; i++)
    {
        summ += array[remains][i];
    }
    std::cout << summ;
}